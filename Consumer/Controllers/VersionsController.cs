using Consumer.Providers;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace Consumer.Controllers
{
    public class VersionsController : Controller
    {
        [HttpGet("{service}")]
        public string Get(string service)
        {
            Task<HttpResponseMessage> response = null;
            if (service == "A")
            {
                IWebApi webApi_A = new WebApi_A();
                response = webApi_A.GetVersion();
            }
            else if (service == "B")
            {
                IWebApi webApi_B = new WebApi_B();
                response = webApi_B.GetVersion();
            }
            return response.Result.StatusCode.ToString();
        }
    }
}