using System.Net.Http;
using System.Threading.Tasks;

namespace Consumer.Providers
{
    public interface IWebApi
    {
        Task<HttpResponseMessage> GetVersion();
    }
}