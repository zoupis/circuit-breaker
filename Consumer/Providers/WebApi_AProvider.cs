using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Consumer.Providers
{
    public class WebApi_A : IWebApi
    {
        public string url { get; set; }
        public HttpClient httpClient { get; set; }

        public WebApi_A()
        {
            url = "http://localhost:5001/Version";
            httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(url);
        }

        public async Task<HttpResponseMessage> GetVersion()
        {
            return await httpClient.GetAsync(url);
        }
    }
}
