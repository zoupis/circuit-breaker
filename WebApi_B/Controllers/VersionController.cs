using Microsoft.AspNetCore.Mvc;
using WebApi_B.Providers;

namespace WebApi_B.Controllers
{
    [Route("[controller]2")]
    public class VersionController : Controller
    {
        private readonly StateProvider _stateProvider;

        public VersionController(StateProvider stateProvider)
        {
            _stateProvider = stateProvider;
        }

        [HttpGet]
        public IActionResult Get()
        {
            if (_stateProvider.IsStateOk)
            {
                return Ok("Version2");
            }
            else
            {
                return StatusCode(500);
            }
        }
    }
}