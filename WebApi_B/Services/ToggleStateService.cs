using System;
using System.Threading;
using System.Threading.Tasks;
using WebApi_B.Providers;

namespace WebApi_B.Services
{
    public class ToggleStateService : HostedService
    {
        private readonly StateProvider _stateProvider;

        public ToggleStateService(StateProvider stateProvider)
        {
            _stateProvider = stateProvider;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromSeconds(50), cancellationToken);
                _stateProvider.ToggleState();
                await Task.Delay(TimeSpan.FromSeconds(15), cancellationToken);
                _stateProvider.ToggleState();
            }
        }
    }
}
