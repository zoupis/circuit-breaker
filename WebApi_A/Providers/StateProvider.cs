using System.Threading;
using System.Threading.Tasks;

namespace WebApi_A.Providers
{
    public class StateProvider
    {
        public bool IsStateOk { get; set; }
        public StateProvider()
        {
            IsStateOk = true;
        }

        public void ToggleState()
        {
            IsStateOk = !IsStateOk;
        }
    }
}
