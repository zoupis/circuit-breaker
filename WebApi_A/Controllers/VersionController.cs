using Microsoft.AspNetCore.Mvc;
using WebApi_A.Providers;

namespace WebApi_A.Controllers
{
    [Route("[controller]")]
    public class VersionController : Controller
    {
        private readonly StateProvider _stateProvider;

        public VersionController(StateProvider stateProvider)
        {
            _stateProvider = stateProvider;
        }

        [HttpGet]
        public IActionResult Get()
        {
            if (_stateProvider.IsStateOk)
            {
                return Ok("Version");
            }
            else
            {
                return StatusCode(500);
            }
        }
    }
}